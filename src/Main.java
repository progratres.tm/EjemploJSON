
public class Main 
{

	public static void main(String[] args) 
	{
		Coordenada casa = new Coordenada("Casa", -34.2356, -54.1245);
		Coordenada facu = new Coordenada("Facu", -34.2215, -54.0654);
		Coordenada trabajo = new Coordenada("Trabajo", -34.2178, -54.0954);

		CoordenadasJSON lasCoordenadas = new CoordenadasJSON();
		
		lasCoordenadas.addCoord(casa);
		lasCoordenadas.addCoord(facu);
		lasCoordenadas.addCoord(trabajo);
		
		String jsonBasico = lasCoordenadas.generarJSONBasico();
		String jsonPretty = lasCoordenadas.generarJSONPretty();
		
		lasCoordenadas.generarJSON(jsonBasico, "Coordenadas.JSON");
		lasCoordenadas.generarJSON(jsonPretty, "CoordenadasPretty.JSON");
		
		CoordenadasJSON leidas = CoordenadasJSON.leerJSON("Coordenadas.JSON");
		for (int i=0; i<leidas.tama�o(); i++)
		{
			System.out.println(leidas.dame(i));
		}

	}
}
