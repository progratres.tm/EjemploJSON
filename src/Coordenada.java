
public class Coordenada 
{
	private String nombre;
	private double latitud;
	private double longitud;
	
	public Coordenada(String nombre, double latitud, double longitud) 
	{
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public double getLatitud() 
	{
		return latitud;
	}

	public double getLongitud() 
	{
		return longitud;
	}

	@Override
	public String toString() 
	{
		return "Coordenada [nombre=" + nombre + ", latitud=" + latitud
				+ ", longitud=" + longitud + "]";
	}	
}
