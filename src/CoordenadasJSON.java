import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class CoordenadasJSON 
{
	private ArrayList<Coordenada> coordenadas;
	
	public CoordenadasJSON()
	{
		coordenadas = new ArrayList<Coordenada>();
	}
	
	public void addCoord(Coordenada c)
	{
		coordenadas.add(c);
	}
	
	public Coordenada dame(int indice)
	{
		return coordenadas.get(indice);
	}
	
	public int tama�o()
	{
		return coordenadas.size();
	}
	
	public String generarJSONBasico()
	{
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);
		
		return json;
	}
	
	public String generarJSONPretty()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		return json;
	}
	
	public void generarJSON(String archivo)
	{
		String json = this.generarJSONPretty();
		
		try{
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void generarJSON(String json, String archivo)
	{
		try
		{
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static CoordenadasJSON leerJSON(String archivo)
	{
		Gson gson = new Gson();
		CoordenadasJSON ret = null;
		
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, CoordenadasJSON.class);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return ret;
	}
}
